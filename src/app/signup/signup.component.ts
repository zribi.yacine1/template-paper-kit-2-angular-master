import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/services/auth.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    test : Date = new Date();
    focus;
    focus1;
    constructor(public auth: AuthService, public activeModal: NgbActiveModal) { }

    ngOnInit() {}
}
