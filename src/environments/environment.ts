// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCqs-Ugr_tCNe9d-76KsdErs7s6kqB8TDQ",
    authDomain: "app-5orda.firebaseapp.com",
    databaseURL: "https://app-5orda.firebaseio.com",
    projectId: "app-5orda",
    storageBucket: "app-5orda.appspot.com",
    messagingSenderId: "729621623321",
    appId: "1:729621623321:web:bc8a3a049ff1f918"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
